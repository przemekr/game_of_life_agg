#include "ctrl/agg_cbox_ctrl.h"

class MenuView : public View
{
public:
   MenuView(App& application): app(application),
   exitApp (40, 20,   150, 40,    "Quit App",  !flip_y),
   exitMenu(40, 60,   150, 80,    "Return  ",  !flip_y),
   music(220, 60,                 "Music  ",  !flip_y)
   {
      exitMenu.background_color(red);
      exitApp.background_color(red);

      music.text_size(15);
      music.text_color(red);
      music.text_thickness(CTRL_TEXT_THICKNESS);
      music.active_color(red);
      music.inactive_color(red);

      add_ctrl(music);
      add_ctrl(exitMenu);
      add_ctrl(exitApp);
   }
   virtual void on_draw()
   {
      int w = app.rbuf_window().width();
      int h = app.rbuf_window().height();
      double scale = 1.0;
      static agg::trans_affine shape_mtx; shape_mtx.reset();
      static agg::trans_affine img_mtx; img_mtx.reset();
      shape_mtx *= agg::trans_affine_scaling(scale);
      shape_mtx *= agg::trans_affine_translation(0, 0);

      img_mtx *= agg::trans_affine_scaling(scale);
      img_mtx *= agg::trans_affine_translation(0, 100);
      img_mtx.invert();

      pixfmt_type pf(app.rbuf_window());;
      renderer_base_type rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      rbase.clear(lgray);

      typedef agg::span_interpolator_linear<agg::trans_affine> interpolator_type;
      interpolator_type interpolator(img_mtx);
      typedef agg::image_accessor_clone<pixfmt_type> img_accessor_type;
      pixfmt_type pixf_img(app.rbuf_img(0));
      img_accessor_type ia(pixf_img);
      typedef agg::span_image_filter_rgba_nn<img_accessor_type, interpolator_type> span_gen_type;
      span_gen_type sg(ia, interpolator);
      agg::span_allocator<color_type> sa;
      ras.move_to_d(0,100);
      ras.line_to_d(w,100);
      ras.line_to_d(w,h);
      ras.line_to_d(0,h);
      agg::render_scanlines_aa(ras, sl, rbase, sa, sg);

      exitMenu.transform(shape_mtx);
      exitApp.transform(shape_mtx);
      agg::render_ctrl(ras, sl, rbase, exitMenu);
      agg::render_ctrl(ras, sl, rbase, exitApp);
      agg::render_ctrl(ras, sl, rbase, music);
   }
   virtual void on_ctrl_change()
   {
      if (exitMenu.status())
      {
         exitMenu.status(false);
         app.changeView("game");
      }
      if (exitApp.status())
      {
         throw 0;
      }
      app.music_on(music.status());
   }
   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_up(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_down(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_move(x, y, (flags & agg::mouse_left) != 0))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
   }

private:
    App& app;
    agg::button_ctrl<agg::rgba8> exitMenu;
    agg::button_ctrl<agg::rgba8> exitApp;
    agg::cbox_ctrl<agg::rgba8>   music;
};
