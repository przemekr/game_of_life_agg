//----------------------------------------------------------------------------
// Anti-Grain Geometry (AGG) - Version 2.5
// A high quality rendering engine for C++
// Copyright (C) 2002-2006 Maxim Shemanarev
// Contact: mcseem@antigrain.com
//          mcseemagg@yahoo.com
//          http://antigrain.com
// 
// AGG is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// AGG is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AGG; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
// MA 02110-1301, USA.
//----------------------------------------------------------------------------

#ifndef AGG_NEXT_CTRL_INCLUDED
#define AGG_NEXT_CTRL_INCLUDED

#include "agg_basics.h"
#include "agg_conv_stroke.h"
#include "agg_trans_affine.h"
#include "agg_color_rgba.h"
#include "ctrl/agg_ctrl.h"

namespace agg
{

    //----------------------------------------------------------next_ctrl_impl
    class next_ctrl_impl : public ctrl
    {
    public:
        next_ctrl_impl(double x, double y, bool flip_y=false);

        bool status() const { return m_status; }
        void status(bool st) { m_status = st; }

        virtual bool in_rect(double x, double y) const;
        virtual bool on_mouse_button_down(double x, double y);
        virtual bool on_mouse_button_up(double x, double y);
        virtual bool on_mouse_move(double x, double y, bool button_flag);
        virtual bool on_arrow_keys(bool left, bool right, bool down, bool up);

        // Vertex soutce interface
        unsigned num_paths() { return 3; };
        void     rewind(unsigned path_id);
        unsigned vertex(double* x, double* y);

    private:
        bool     m_status;
        double   m_vx[32];
        double   m_vy[32];
        unsigned m_idx;
        unsigned m_vertex;
    };


    //----------------------------------------------------------next_ctrl_impl
    template<class ColorT> class next_ctrl : public next_ctrl_impl
    {
    public:
        next_ctrl(double x, double y, bool flip_y=false) :
            next_ctrl_impl(x, y, flip_y),
            m_inactive_color(rgba(0.0, 0.0, 0.0, 0.0)),
            m_active_color(rgba(1.0, 0.0, 1.0, 0.8))
        {
            m_colors[0] = &m_inactive_color;
            m_colors[1] = &m_active_color;
            m_colors[2] = &m_active_color;
        }
          
        void inactive_color(const ColorT& c) { m_inactive_color = c; }
        void active_color(const ColorT& c) { m_active_color = c; }

        const ColorT& color(unsigned i) const { return *m_colors[i]; } 

    private:
        next_ctrl(const next_ctrl<ColorT>&);
        const next_ctrl<ColorT>& operator = (const next_ctrl<ColorT>&);

        ColorT m_inactive_color;
        ColorT m_active_color;
        ColorT* m_colors[3];
    };
}

#endif
