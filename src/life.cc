/*
 * Game of Life AGG, the game of life simulation.
 * Copyright 2013 Przemyslaw Rzepecki
 * Contact: przemekr@sdfeu.org
 * 
 * This file is part of GameOfLifeAgg.
 * 
 * GameOfLifeAgg is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * GameOfLifeAgg is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * GameOfLifeAgg.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "life.h"
#define MOD(x, m) ((x+m)%m)

void Universe::tick()
{
   Universe newUni(w, h);
   for (iterator loc = begin(); loc != end(); loc++)
   {
      /* Check the neighborhood */
      for (int i = -1; i <= 1; i++)
         for (int j = -1; j <= 1; j++)
         {
            if (!i && !j)
               continue;

            Loc n(MOD(loc->first.x+i, w), MOD(loc->first.y+j, h));
            if (count(n))
               continue;

            if (getNoNeighbors(n) == 3)
               newUni[n] = newLife(n);
         }

      /* Check current cell */
      switch (getNoNeighbors(loc->first))
      {
         case 2:
         case 3:
            newUni[loc->first] = getOld((*this)[loc->first]);
            break;
         default:
            break;
      }
   }
   *this = newUni;
}

void Universe::resize(int width, int hight)
{
   shift((width-w)/2, (hight-h)/2);
   w = width; h = hight;
   for (iterator loc = begin(); loc != end();)
   {
      if (loc->first.x > w || loc->first.y > h)
         erase(loc++);
      else
         loc++;
   }
}
void Universe::shift(int dx, int dy)
{
   Universe newUni(w, h);
   for (iterator l = begin(); l != end(); l++)
   {
      Loc n(MOD(l->first.x+dx, w), MOD(l->first.y+dy, h));
      newUni[n] = l->second;
   }
   *this = newUni;
}

int Universe::getNoNeighbors(const Loc& l)
{
   int neighbors = 0;
   for (int i = -1; i <= 1; i++)
      for (int j = -1; j <= 1; j++)
      {
         if (
               (j || i) /* count only neighbors not loc */
               && count(Loc(MOD(l.x+i, w), MOD(l.y+j, h)))
            )
            neighbors++;
      }
   return neighbors;
}

Life Universe::getOld(Life c)
{
   if (++c.age % 15 == 0)
      ++c.gender;
   return c;
}

Life Universe::newLife(Loc l)
{
   int dx; int dy; int gender[3] = {0};
   int max = 0; int dominant = 0;
   for (dx = -1; dx <= 1; dx++)
      for (dy = -1; dy <= 1; dy++)
      {
         Loc n = Loc((w+l.x+dx)%w, (h+l.y+dy)%h);
         iterator i = find(n);
         if (i == end())
            continue;

         int count = ++gender[i->second.gender%3];
         if (count > max)
         {
            dominant = i->second.gender;
            max = count;
         }
      }
   Life c; c.age = 1, c.gender = dominant;
   return c;
}

void Universe::setState(const char* state, int x, int y)
{
   clear();
   int i = x; int j = y;
   Life newXLife = {1, 0};
   Life newYLife = {1, 1};
   Life newZLife = {1, 2};
   while(*state != '\0')
   {
      switch(*state++)
      {
         case 'X':
            (*this)[Loc(i++, j)] = newXLife;
            break;
         case 'Y':
            (*this)[Loc(i++, j)] = newYLife;
            break;
         case 'Z':
            (*this)[Loc(i++, j)] = newZLife;
            break;
         case '\n':
            j++; i = x;
            break;
         default:
            i++;
            break;
      }
   }
}
