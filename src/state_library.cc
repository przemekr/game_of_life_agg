/*
 * Game of Life AGG, the game of life simulation.
 * Copyright 2013 Przemyslaw Rzepecki
 * Contact: przemekr@sdfeu.org
 * 
 * This file is part of GameOfLifeAgg.
 * 
 * GameOfLifeAgg is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * GameOfLifeAgg is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * GameOfLifeAgg.  If not, see <http://www.gnu.org/licenses/>.
 */

const char* library[] = {
 "",

 "  ZZ   ZZ       \n"
 " ZZZZ ZZZZ      \n"
 "ZZZZZZZZZZZ     \n"
 " ZZZZZZZZZ      \n"
 "  ZZZZZZZ       \n"
 "   ZZZZZ        \n"
 "    ZZZ         \n"
 "     Z          \n",

 "                \n"
 "                \n"
 "    X           \n"
 "    XXX         \n"
 "     X          \n"
 "                \n"
 "                \n"
 "                \n",
 
 "   X  X        \n"
 "  X XX X       \n"
 "   X  X        \n"
 "   X  X        \n"
 "  X XX X       \n"
 "   X  X        \n"
 "               \n",

 "   YYY          \n"
 "   Y Y          \n"
 "   Y Y          \n"
 "                \n"
 "   Y Y          \n"
 "   Y Y          \n"
 "   YYY          \n"
 "                \n",

 "                        Z            \n"
 "                      Z Z            \n"
 "            XX      ZZ            ZZ \n"
 "           X   X    ZZ            ZZ \n"
 "XX        X     X   ZZ               \n"
 "XX        X   X XX    Z Z            \n"
 "          X     X       Z            \n"
 "           X   X                     \n"
 "            XX                       \n",

 "XXXXXXXX YYYYY   YYY      ZZZZZZZ XXXXX",
 
 "                \n"
 "                \n"
 " YYY X          \n"
 " Y              \n"
 "    XX          \n"
 "  XX X          \n"
 " X X X          \n"
 "                \n"
};
int library_size = sizeof(library)/sizeof(char*);
