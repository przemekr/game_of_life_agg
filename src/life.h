/*
 * Game of Life AGG, the game of life simulation.
 * Copyright 2013 Przemyslaw Rzepecki
 * Contact: przemekr@sdfeu.org
 * 
 * This file is part of GameOfLifeAgg.
 * 
 * GameOfLifeAgg is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * GameOfLifeAgg is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * GameOfLifeAgg.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <map>


typedef struct {int age; int gender;} Life;
struct Loc {int x; int y;
   Loc(int i, int j): x(i), y(j) {}
   bool operator<(const Loc& o) const {
      if (x < o.x) return true;
      if (x > o.x) return false;
      if (y < o.y) return true;
      return false;
   }
};

class Universe: public std::map<Loc, Life>
{
public:
   Universe (int width, int hight):
      w(width), h(hight) { };
   virtual ~Universe () {};
   void tick();
   void resize(int width, int hight);
   void shift(int x, int y);
   void setState(const char* state, int x, int y);
   Life newLife(Loc l);

private:
   int w, h;
   int getNoNeighbors(const Loc& l);
   Life getOld(Life c);
};
