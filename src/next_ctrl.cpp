//----------------------------------------------------------------------------
// Anti-Grain Geometry (AGG) - Version 2.5
// A high quality rendering engine for C++
// Copyright (C) 2002-2006 Maxim Shemanarev
// Contact: mcseem@antigrain.com
//          mcseemagg@yahoo.com
//          http://antigrain.com
// 
// AGG is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// AGG is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AGG; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
// MA 02110-1301, USA.
//----------------------------------------------------------------------------
#include <stdio.h>

#include <string.h>
#include "next_ctrl.h"


namespace agg
{
    //------------------------------------------------------------------------
    next_ctrl_impl::next_ctrl_impl(double x, double y, bool flip_y) :
        ctrl(x, y, x + 9.0 * 1.5, y + 9.0 * 1.5, flip_y),
        m_status(false)
    {
    }

    //------------------------------------------------------------------------
    bool next_ctrl_impl::on_mouse_button_down(double x, double y)
    {
        inverse_transform_xy(&x, &y);
        if(x >= m_x1 && y >= m_y1 && x <= m_x2 && y <= m_y2)
        {
            m_status = !m_status;
            return true;
        }
        return false;
    }


    //------------------------------------------------------------------------
    bool next_ctrl_impl::on_mouse_move(double, double, bool)
    {
        return false;
    }

    //------------------------------------------------------------------------
    bool next_ctrl_impl::in_rect(double x, double y) const
    {
        inverse_transform_xy(&x, &y);
        return x >= m_x1 && y >= m_y1 && x <= m_x2 && y <= m_y2;
    }

    //------------------------------------------------------------------------
    bool next_ctrl_impl::on_mouse_button_up(double, double)
    {
        return false;
    }

    //------------------------------------------------------------------------
    bool next_ctrl_impl::on_arrow_keys(bool, bool, bool, bool)
    {
        return false;
    }


    //------------------------------------------------------------------------
    void next_ctrl_impl::rewind(unsigned idx)
    {
        m_idx = idx;

        double d2;
        double t;

        switch(idx)
        {
        default:
        case 0:                 // Border
            m_vertex = 0;
            m_vx[0] = m_x1; 
            m_vy[0] = m_y1;
            m_vx[1] = m_x2;
            m_vy[1] = m_y1;
            m_vx[2] = m_x2;
            m_vy[2] = m_y2;
            m_vx[3] = m_x1; 
            m_vy[3] = m_y2;
            m_vx[4] = m_x1 + 1; 
            m_vy[4] = m_y1 + 1; 
            m_vx[5] = m_x1 + 1; 
            m_vy[5] = m_y2 - 1;
            m_vx[6] = m_x2 - 1;
            m_vy[6] = m_y2 - 1;
            m_vx[7] = m_x2 - 1;
            m_vy[7] = m_y1 + 1; 
            break;

        case 1:                 // next >>
            m_vertex = 0;
            d2 = (m_y2 - m_y1) / 2.0;
            t = 0.5;
            m_vx[0] = m_x1 + t; 
            m_vy[0] = m_y1 + t;
            m_vx[1] = m_x1 + d2 + t;
            m_vy[1] = m_y1 + d2;
            m_vx[2] = m_x1 + t;
            m_vy[2] = m_y2 - t;

            m_vx[3] = m_x1 + t + d2; 
            m_vy[3] = m_y1 + t;
            m_vx[4] = m_x2 - t;
            m_vy[4] = m_y1 + d2;
            m_vx[5] = m_x1 + t + d2;
            m_vy[5] = m_y2 - t;
            break;
        }
    }


    //------------------------------------------------------------------------
    unsigned next_ctrl_impl::vertex(double* x, double* y)
    {
        unsigned cmd = path_cmd_line_to;
        switch (m_idx)
        {
        case 0:
            if (m_vertex == 0 || m_vertex == 4) cmd = path_cmd_move_to;
            if (m_vertex >= 8) cmd = path_cmd_stop;
            *x = m_vx[m_vertex];
            *y = m_vy[m_vertex];
            m_vertex++;
            break;

        case 1:
            if (m_vertex == 0 || m_vertex == 3) cmd = path_cmd_move_to;
            if (m_vertex >= 6) cmd = path_cmd_stop;
            *x = m_vx[m_vertex];
            *y = m_vy[m_vertex];
            m_vertex++;
            break;

        default:
            cmd = path_cmd_stop;
            break;
        }

        if (!is_stop(cmd))
        {
            transform_xy(x, y);
        }
        return cmd;
    }
}
