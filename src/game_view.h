#include "life.h"
#include "pause_ctrl.h"
#include "next_ctrl.h"
#include "state_library.h"

const agg::rgba palete[] = {green, yellow, red};
static agg::trans_affine shape_mtx;

class GameView : public AppView
{
public:
   GameView(App& app): AppView(app),
      speed_ctr(140, 10, 300, 25, !flip_y),
      size_ctr(320, 10, 480, 25, !flip_y),
      pause_ctr(10, 1, !flip_y),
      next_ctr(30, 1, !flip_y),
      menu(500,  10, 580, 25,   "MENU", !flip_y),
      menubarsize(35),
      universe(w, h)
   {
      step = 0;
      nextInitial = 0;
      lx = ly = 0;
      mouse_moved = false;
      w = int(rbuf_window().width());
      h = int(rbuf_window().height()) - menubarsize;
      speed_ctr.range(1, 10);
      speed_ctr.num_steps(50);
      speed_ctr.value(2);
      speed_ctr.label("Speed=%1.1f");
      speed_ctr.no_transform();
      speed_ctr.background_color(transp);
      speed_ctr.text_thickness(CTRL_TEXT_THICKNESS);
      size_ctr.range(1, 40);
      size_ctr.num_steps(29);
      size_ctr.value(12);
      size_ctr.label("Size=%1.0f");
      size_ctr.no_transform();
      size_ctr.background_color(transp);
      size_ctr.text_thickness(CTRL_TEXT_THICKNESS);
      csize = int(size_ctr.value());
      universe.setState(library[nextInitial++%library_size],
            w/3/csize, h/3/csize);

      /* enlarge the pause and next ctrls */
      shape_mtx *= agg::trans_affine_scaling(1.8);
      shape_mtx *= agg::trans_affine_translation(0, 0);
      pause_ctr.transform(shape_mtx);
      next_ctr.transform(shape_mtx);

      menu.background_color(red);
   }

   virtual void on_ctrl_change()
   {
      force_redraw();
      wait_mode(!pause_ctr.status());

      if (next_ctr.status())
      {
         next_ctr.status(0);
         universe.setState(library[nextInitial++%library_size],
               w/3/csize, h/3/csize);
      }

      if (csize != int(size_ctr.value()))
      {
         csize = int(size_ctr.value());
         universe.resize(w/csize, h/csize);
      }

      if (menu.status())
      {
         menu.status(false);
         app.changeView("menu");
      }
   }

   int max_fps()
   {
      return (int)speed_ctr.value()*5;
   }

   void update(long elapsed_time)
   {
      universe.tick();
      step++;
   }

   virtual void on_resize(int, int)
   {
      force_redraw();
      w = int(rbuf_window().width());
      h = int(rbuf_window().height()) - menubarsize;
      universe.resize(w/csize, h/csize);
   }

   int pixTol(int pixel, int length, int offset = 0)
   {
      pixel -= offset;
      length /= csize;
      pixel  /= csize;
      int r = length-1 - pixel;
      return r % length;
   }
   int lTopix(int l, int length, int offset = 0)
   {
      length /= csize;
      int r = length-1;
      r -= l;
      r %= length; if (r < 0) r += length;
      return r*csize+offset;
   }

   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      lx = ly = 0;
      speed_ctr.on_mouse_button_up(x, y);
      size_ctr.on_mouse_button_up(x, y);
      next_ctr.on_mouse_button_up(x, y);

      if (mouse_moved)
      {
         mouse_moved = false;
         return;
      }

      if (inUniverse(x,y))
      {
         int i = pixTol(x, w);
         int j = pixTol(y, h, menubarsize);
         if (universe.count(Loc(i, j)))
            universe.erase(Loc(i, j));
         else
            universe[Loc(i, j)] = universe.newLife(Loc(i, j));
      }
      force_redraw();
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (speed_ctr.on_mouse_button_down(x, y)
            || size_ctr.on_mouse_button_down(x, y)
            || pause_ctr.on_mouse_button_down(x, y)
            || next_ctr.on_mouse_button_down(x, y)
            || menu.on_mouse_button_down(x, y))
      {
         on_ctrl_change();
         return;
      }
      lx = x; ly = y;
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      if (flags & agg::mouse_left)
      {
         if (speed_ctr.on_mouse_move(x, y, true)
               || size_ctr.on_mouse_move(x, y, true))
         {
            on_ctrl_change();
            return;
         }

         if (abs(x-lx) > csize || abs(y-ly) > csize)
         {
            mouse_moved = true;
            universe.shift((lx-x)/csize, (ly-y)/csize);
            lx = x; ly = y;
            force_redraw();
         }
      }
   }
   virtual void on_multi_gesture(float x, float y,
         float dTheta, float dDist, int numFingers)
   {
      static double d = 0;
      d += dDist*38;
      if (abs(d) > 1)
      {
         size_ctr.value(size_ctr.value()+(int)d);
         d = 0;
         on_ctrl_change();
      }
   }


   virtual void on_draw()
   {
      pixfmt_type pf(rbuf_window());;
      agg::renderer_base<pixfmt_type> rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      rbase.clear(lgray);

      if (csize > 4)
      {
         int i;
         for (i = 0; i< h/csize; i++)
            rbase.blend_hline(0, menubarsize+csize*i, w, lblue, 64);

         for (i = 0; i< w/csize; i++)
            rbase.blend_vline(csize*i, menubarsize, h+menubarsize, lblue, 64);
      }

      for (Universe::iterator i = universe.begin(); i != universe.end(); i++)
         drawCell(rbase, i->first.x, i->first.y, i->second);

      char buf[50];
      sprintf(buf, "%d", step);
      app.draw_text(10, h-50, 30, buf);
      agg::render_ctrl(ras, sl, rbase, speed_ctr);
      agg::render_ctrl(ras, sl, rbase, size_ctr);
      agg::render_ctrl(ras, sl, rbase, pause_ctr);
      agg::render_ctrl(ras, sl, rbase, next_ctr);
      agg::render_ctrl(ras, sl, rbase, menu);
   }
private:
    Universe universe;
    int step;
    int nextInitial;
    int csize;
    int menubarsize;
    int w, h;
    int lx, ly;
    bool mouse_moved;
    agg::slider_ctrl<agg::rgba8> speed_ctr;
    agg::slider_ctrl<agg::rgba8> size_ctr;
    agg::pause_ctrl<agg::rgba8>  pause_ctr;
    agg::next_ctrl<agg::rgba8>   next_ctr;
    agg::button_ctrl<agg::rgba8> menu;

    bool inUniverse(int x, int y)
    {
       return y > menubarsize;
    }

    void drawCell(agg::renderer_base<pixfmt_type>& rbase,
          int x, int y, Life c)
    {
       rbase.blend_bar(
             lTopix(x, w),
             lTopix(y, h, menubarsize),
             lTopix(x, w)+csize,
             lTopix(y, h, menubarsize)+csize,
             palete[c.gender%3], 170-(10*c.age%150));
    }
};
